﻿using System.Collections.Generic;
using Entitas;

namespace MercuryEngine.Rules {
  public sealed class RespawnIfOutOfBoundsSystem : ReactiveSystem<PhysicsEntity> {
    private readonly PhysicsContext physics;

    public RespawnIfOutOfBoundsSystem(Contexts contexts) : base(contexts.physics) {
      this.physics = contexts.physics;
    }

    protected override ICollector<PhysicsEntity> GetTrigger(IContext<PhysicsEntity> context) {
      return context.CreateCollector(PhysicsMatcher.Position);
    }

    protected override bool Filter(PhysicsEntity entity) {
      return entity.hasStageCollider && entity.hasPosition;
    }

    protected override void Execute(List<PhysicsEntity> entities) {
      if (!this.physics.hasOutOfBoundsBox) return;
      var box = this.physics.outOfBoundsBox;
      var leftBound = -box.sizeLeft;
      var rightBound = box.sizeRight;
      var topBound = box.sizeAbove;
      var bottomBound = -box.sizeBelow;
      foreach (var entity in entities) {
        //TODO: This is wrong.
//        var x = entity.position.X;
//        var y = entity.position.Y;
//        var width = entity.stageCollider.Width;
//        var height = entity.stageCollider.Height;
//        var left = x - width;
//        var right = x + width;
//        var bottom = y - height;
//        var top = y + height;
//        var outOfBounds = left < leftBound || right > rightBound || bottom < bottomBound || top > topBound;
//        if (!outOfBounds) continue;
//        entity.ReplacePosition(0, 0);
//        entity.RemovePreviousFramePosition();
      }
    }
  }
}