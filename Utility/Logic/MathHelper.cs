using System.Diagnostics.CodeAnalysis;
using SysMath = System.Math;

namespace MercuryEngine.Utility {
  [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
  public static class MathHelper {
    public const float Pi = (float) SysMath.PI;

    public static float LargestInnerSquare(double size) {
      return (float) SysMath.Sqrt(SysMath.Pow(size, 2) / 2.0);
    }

    public static (float x, float y) RotatePoint(float x, float y, float radians,
                                                 float originX = 0, float originY = 0) {
      return (
        originX + x * Cos(radians) - y * Sin(radians),
        originY + x * Sin(radians) + y * Cos(radians)
      );
    }

    public static float Cos(float f) => (float) SysMath.Cos(f);
    public static float Sin(float f) => (float) SysMath.Sin(f);
    public static float Atan2(float y, float x) => (float) SysMath.Atan2(y, x);
    public static float DegreesToRadians(float degrees) => Pi / 180 * degrees;


    public static float Clamp(float value, float min, float max) {
      return value < (double) min
        ? min
        : value > (double) max
          ? max
          : value;
    }

    public static float Clamp01(float value) => Clamp(value, 0, 1);


    public static (float x, float y) Normalize(float x, float y) {
      var magnitude = Magnitude(x, y);
      const double epsilon = 9.99999974737875E-06;
      return magnitude > epsilon
        ? (x / magnitude, y / magnitude)
        : (0, 0);
    }

    public static float Magnitude(float x, float y) {
      {
        return Sqrt((float) (x * (double) x + (double) y * y));
      }
    }

    public static float Sqrt(float f) {
      return (float) SysMath.Sqrt(f);
    }

    public static float CeilingIfDefaultOtherwiseClamp(float value, float floor, float ceiling) {
      // ReSharper disable once CompareOfFloatsByEqualityOperator
      // Only cares if value is uninitialised, so exactly 0
      return value == default ? ceiling : Clamp(value, floor, ceiling);
    }

    public static (float min, float max) ClampDeadzones(float min, float max,
                                                        float floor, float ceiling) {
      max = CeilingIfDefaultOtherwiseClamp(max, floor, ceiling);

      min = Clamp(min, floor, max);
      return (min, max);
    }
  }
}