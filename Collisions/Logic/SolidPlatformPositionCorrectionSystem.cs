﻿using System.Collections.Generic;
using Entitas;

namespace MercuryEngine.Collisions {
  public class SolidPlatformPositionCorrectionSystem : ReactiveSystem<PhysicsEntity> {
    private readonly IGroup<PhysicsEntity> solidPlatforms;

    public SolidPlatformPositionCorrectionSystem(Contexts contexts) : base(contexts.physics) {
      this.solidPlatforms = contexts.physics.GetGroup(PhysicsMatcher.SolidPlatform);
    }

    protected override ICollector<PhysicsEntity> GetTrigger(IContext<PhysicsEntity> context) {
      return context.CreateCollector(
        PhysicsMatcher.AllOf(
          PhysicsMatcher.Position,
          PhysicsMatcher.StageCollider
        )
      );
    }

    protected override bool Filter(PhysicsEntity entity) {
      return entity.hasPosition && entity.hasStageCollider;
    }

    protected override void Execute(List<PhysicsEntity> entities) {
      foreach (var platform in this.solidPlatforms)
      foreach (var entity in entities) { }
    }
  }
}