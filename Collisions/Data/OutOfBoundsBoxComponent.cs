﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace MercuryEngine.Collisions {
  [Physics, Unique]
  public sealed class OutOfBoundsBoxComponent : IComponent {
    public float sizeLeft, sizeRight, sizeAbove, sizeBelow;
  }
}