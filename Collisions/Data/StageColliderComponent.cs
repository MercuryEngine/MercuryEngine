﻿using Entitas;

namespace MercuryEngine.Collisions {
  [Physics]
  public sealed class StageColliderComponent : IComponent {
    public float x1, y1;
    public float x2, y2;
    public float x3, y3;
    public float x4, y4;
  }
}