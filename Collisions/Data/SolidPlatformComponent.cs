﻿using Entitas;

namespace MercuryEngine.Collisions {
  [Physics]
  public sealed class SolidPlatformComponent : IComponent {
    public float left, right, bottom, top;
  }
}