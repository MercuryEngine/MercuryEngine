using System;

namespace MercuryEngine.Meta {
  public static class DiagnosticsHelper {
    public static string UnknownEnumValueMessage<T>(T value) where T : Enum {
      return $"Unexpected {value.GetType().Name} '{value}'";
    }
  }
}