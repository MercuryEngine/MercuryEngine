using MercuryEngine.Config;
using MercuryEngine.Drawing;
using MercuryEngine.Input;

namespace MercuryEngine.Meta {
  public sealed class Services : IServices {
    public Services(IConfigService config, IInputService input,
                    IDrawPrimitives2DService drawPrimitives2D) {
      this.config = config;
      this.input = input;
      this.drawPrimitives2D = drawPrimitives2D;
    }

    public IConfigService config { get; }
    public IInputService input { get; }
    public IDrawPrimitives2DService drawPrimitives2D { get; }
  }
}