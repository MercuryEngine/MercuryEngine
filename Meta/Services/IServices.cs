using MercuryEngine.Config;
using MercuryEngine.Drawing;
using MercuryEngine.Input;

namespace MercuryEngine.Meta {
  public interface IServices {
    IConfigService config { get; }
    IInputService input { get; }
    IDrawPrimitives2DService drawPrimitives2D { get; }
  }
}