using Entitas.CodeGeneration.Attributes;

namespace MercuryEngine.Meta {
  [Draw, Input, FlagPrefix("should")]
  public sealed class DestroyComponent : Entitas.IComponent { }
}