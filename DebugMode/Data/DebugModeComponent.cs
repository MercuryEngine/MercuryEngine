﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace MercuryEngine.DebugMode {
  [Physics, Unique]
  public sealed class DebugModeComponent : IComponent { }
}