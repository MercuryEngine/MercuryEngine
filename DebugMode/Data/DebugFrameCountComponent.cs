﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace MercuryEngine.DebugMode {
  [Debug, Unique]
  public sealed class DebugFrameCountComponent : IComponent {
    public uint value;
  }
}