﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace MercuryEngine.DebugMode {
  [Debug, Unique]
  public sealed class DebugPauseComponent : IComponent { }
}