﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace MercuryEngine.DebugMode {
  [Debug, Unique]
  public sealed class TotalFrameCountComponent : IComponent {
    public uint value;
  }
}