using System.Collections.Generic;
using Entitas;


namespace MercuryEngine {
  public interface IDestroyableEntity : IEntity, IDestroyEntity { }

  public abstract class DestroySystem<TEntity> : ICleanupSystem where TEntity : class, IEntity {
    private readonly IGroup<TEntity> group;
    private readonly List<TEntity> buffer;

    protected DestroySystem(IContext<TEntity> context, IMatcher<TEntity> destroyMatcher) {
      this.group = context.GetGroup(destroyMatcher);
      this.buffer = new List<TEntity>(this.group.count);
    }

    public void Cleanup() {
      foreach (var entity in this.group.GetEntities(this.buffer)) {
        entity.Destroy();
      }
    }
  }
}