namespace MercuryEngine.Physics {
  public abstract class Vector2Component : Entitas.IComponent {
    public float x { get; set; }
    public float y { get; set; }
  }

  [Physics]
  public sealed class PositionComponent : Vector2Component { }

  [Physics]
  public sealed class VelocityComponent : Vector2Component { }

  [Physics]
  public sealed class AccelerationComponent : Vector2Component { }

  [Physics]
  public sealed class PreviousFramePositionComponent : Vector2Component { }
}