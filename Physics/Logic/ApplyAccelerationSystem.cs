﻿using Entitas;

namespace MercuryEngine.Physics {
  public sealed class ApplyAccelerationSystem : IExecuteSystem {
    private readonly IGroup<PhysicsEntity> group;

    public ApplyAccelerationSystem(Contexts contexts) {
      this.group = contexts.physics.GetGroup(
        PhysicsMatcher.AllOf(
          PhysicsMatcher.Velocity,
          PhysicsMatcher.Acceleration
        )
      );
    }

    public void Execute() {
      foreach (var entity in this.group) {
        var velocity = entity.velocity;
        var acceleration = entity.acceleration;
        entity.ReplaceVelocity(
          velocity.x + acceleration.x,
          velocity.y + acceleration.y
        );
      }
    }
  }
}