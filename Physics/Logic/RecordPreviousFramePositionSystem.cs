﻿using System.Collections.Generic;
using Entitas;

namespace MercuryEngine.Physics {
  public sealed class RecordPreviousFramePositionSystem : ReactiveSystem<PhysicsEntity> {
    public RecordPreviousFramePositionSystem(Contexts contexts) : base(contexts.physics) { }

    protected override ICollector<PhysicsEntity> GetTrigger(IContext<PhysicsEntity> context) {
      return context.CreateCollector(PhysicsMatcher.Position);
    }

    protected override bool Filter(PhysicsEntity entity) {
      return entity.hasPosition;
    }

    protected override void Execute(List<PhysicsEntity> entities) {
      foreach (var entity in entities) {
        var x = entity.position.x;
        var y = entity.position.y;
        entity.ReplacePreviousFramePosition(x, y);
      }
    }
  }
}