﻿using Entitas;

namespace MercuryEngine.Physics {
  public sealed class ApplyVelocitySystem : IExecuteSystem {
    private readonly IGroup<PhysicsEntity> group;

    public ApplyVelocitySystem(Contexts contexts) {
      this.group = contexts.physics.GetGroup(
        PhysicsMatcher.AllOf(
          PhysicsMatcher.Position,
          PhysicsMatcher.Velocity
        )
      );
    }

    public void Execute() {
      foreach (var entity in this.group) {
        var position = entity.position;
        var velocity = entity.velocity;
        entity.ReplacePosition(
          position.x + velocity.x,
          position.y + velocity.y
        );
      }
    }
  }
}