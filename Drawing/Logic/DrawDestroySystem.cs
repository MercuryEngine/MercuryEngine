public partial class DrawEntity : MercuryEngine.IDestroyableEntity { }

namespace MercuryEngine.Drawing {
  public sealed class DrawDestroySystem : DestroySystem<DrawEntity> {
    public DrawDestroySystem(Contexts contexts) : base(contexts.draw, DrawMatcher.Destroy) { }
  }
}