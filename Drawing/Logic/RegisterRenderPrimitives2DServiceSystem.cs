using Entitas;

namespace MercuryEngine.Drawing {
  public sealed class RegisterRenderPrimitives2DServiceSystem : IInitializeSystem {
    private readonly MetaContext context;
    private readonly IDrawPrimitives2DService service;

    public RegisterRenderPrimitives2DServiceSystem(Contexts contexts, IDrawPrimitives2DService service) {
      this.context = contexts.meta;
      this.service = service;
    }

    public void Initialize() {
      this.context.SetRenderPrimitives2DService(this.service);
    }
  }
}