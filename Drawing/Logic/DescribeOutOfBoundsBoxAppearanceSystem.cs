using Entitas;

namespace MercuryEngine.Drawing {
  public sealed class DescribeOutOfBoundsBoxAppearanceSystem : IExecuteSystem {
    private readonly DrawContext draw;
    private readonly PhysicsContext physics;

    public DescribeOutOfBoundsBoxAppearanceSystem(Contexts contexts) {
      this.draw = contexts.draw;
      this.physics = contexts.physics;
    }

    public void Execute() {
      var box = this.physics.outOfBoundsBox;
      var (width, height) = (
        box.sizeLeft + box.sizeRight,
        box.sizeBelow + box.sizeAbove
      );
      var (x, y) = (
        (box.sizeRight - box.sizeLeft) / 2,
        (box.sizeAbove - box.sizeBelow) / 2
      );
      var (red, green, blue) = (255, 255, 255);

      var entity = this.draw.CreateEntity();
      entity.AddPrimitiveShape2D(PrimitiveShape2DType.WireQuad);
      entity.AddScale(width, height);
      entity.AddPosition(x, y);
      entity.AddColor(red, green, blue);
      entity.shouldDestroy = true;
    }
  }
}