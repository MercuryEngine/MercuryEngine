using System.Diagnostics;
using Entitas;
using MercuryEngine.Meta;
using Priority_Queue;

namespace MercuryEngine.Drawing {
  public sealed class DrawPrimitives2DSystem : IExecuteSystem {
    private readonly MetaContext meta;
    private readonly IGroup<DrawEntity> entities;

    public DrawPrimitives2DSystem(Contexts contexts) {
      this.meta = contexts.meta;
      this.entities = contexts.draw.GetGroup(DrawMatcher.PrimitiveShape2D);
    }

    public void Execute() {
      var queue = new FastPriorityQueue<DrawEntityNode>(this.entities.count);

      foreach (var entity in this.entities) {
        var priority = entity.hasZOrder ? entity.zOrder.value : 0;
        var node = new DrawEntityNode(entity);
        queue.Enqueue(node, priority);
      }

      var service = this.meta.renderPrimitives2DService.instance;
      var drawing = false;
      while (queue.Count > 0) {
        if (!drawing) {
          service.Begin();
          drawing = true;
        }

        var entity = queue.Dequeue().entity;
        var shape = entity.primitiveShape2D.type;
        DrawEntityImmediately(entity, shape);
      }

      if (drawing) service.End();
    }

    private void DrawEntityImmediately(DrawEntity entity, PrimitiveShape2DType shape) {
      var service = this.meta.renderPrimitives2DService.instance;
      switch (shape) {
        case PrimitiveShape2DType.WireQuad: {
          DrawWireQuadEntity(entity, service);
          return;
        }
        default:
          Debug.Fail(DiagnosticsHelper.UnknownEnumValueMessage(shape));
          return;
      }
    }

    private static void DrawWireQuadEntity(DrawEntity entity, IDrawPrimitives2DService service) {
      var center = (entity.position.x, entity.position.y);
      var degrees = entity.hasRotation ? entity.rotation.degrees : 0;
      var dimensions = (entity.scale.x, entity.scale.y);
      var color = (entity.color.red, entity.color.green, entity.color.blue);
      service.DrawWireQuad(center, degrees, dimensions, color);
    }

    private sealed class DrawEntityNode : FastPriorityQueueNode {
      public readonly DrawEntity entity;

      public DrawEntityNode(DrawEntity entity) {
        this.entity = entity;
      }
    }
  }
}