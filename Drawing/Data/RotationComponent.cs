namespace MercuryEngine.Drawing {
  [Draw]
  public sealed class RotationComponent : Entitas.IComponent {
    public float degrees;
  }
}