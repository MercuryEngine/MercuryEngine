using Entitas.CodeGeneration.Attributes;

namespace MercuryEngine.Drawing {
  [Meta, Unique]
  public sealed class RenderPrimitives2DServiceComponent : Entitas.IComponent {
    public IDrawPrimitives2DService instance;
  }
}