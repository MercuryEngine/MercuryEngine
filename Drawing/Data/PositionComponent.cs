namespace MercuryEngine.Drawing {
  [Draw]
  public sealed class PositionComponent : Entitas.IComponent {
    public float x, y;
  }
}