namespace MercuryEngine.Drawing {
  [Draw]
  public sealed class ScaleComponent : Entitas.IComponent {
    public float x, y;
  }
}