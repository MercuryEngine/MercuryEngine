namespace MercuryEngine.Drawing {
  [Draw]
  public sealed class ColorComponent : Entitas.IComponent {
    public float red, green, blue;
  }
}