namespace MercuryEngine.Drawing {
  public interface IDrawPrimitives2DService {
    void DrawWireQuad((float x, float y) center, float rotationDegrees,
                      (float width, float height) dimensions,
                      (float red, float green, float blue) color);

    void Begin();
    void End();
  }
}