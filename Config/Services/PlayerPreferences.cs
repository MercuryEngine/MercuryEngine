using System;

namespace MercuryEngine.Config {
  public sealed class PlayerPreferences {
    public readonly Guid guid;
    public readonly string tag;
    public readonly float leftStickInnerDeadzone, leftStickOuterDeadzone;
    public readonly float rightStickInnerDeadzone, rightStickOuterDeadzone;
    public readonly float leftTriggerMin, leftTriggerMax;
    public readonly float rightTriggerMin, rightTriggerMax;

    public PlayerPreferences(Guid guid, string tag,
                             float leftStickInnerDeadzone, float leftStickOuterDeadzone,
                             float rightStickInnerDeadzone, float rightStickOuterDeadzone,
                             float leftTriggerMin, float leftTriggerMax,
                             float rightTriggerMin, float rightTriggerMax) {
      this.guid = guid;
      this.tag = tag;
      this.leftStickInnerDeadzone = leftStickInnerDeadzone;
      this.leftStickOuterDeadzone = leftStickOuterDeadzone;
      this.rightStickInnerDeadzone = rightStickInnerDeadzone;
      this.rightStickOuterDeadzone = rightStickOuterDeadzone;
      this.leftTriggerMin = leftTriggerMin;
      this.leftTriggerMax = leftTriggerMax;
      this.rightTriggerMin = rightTriggerMin;
      this.rightTriggerMax = rightTriggerMax;
    }
  }
}