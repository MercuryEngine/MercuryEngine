using System;
using System.Collections.Generic;

namespace MercuryEngine.Config {
  public interface IConfigService {
    PlayerPreferences GetPreferences(Guid player);
    IEnumerable<Guid> GetPlayerGuids();
  }
}