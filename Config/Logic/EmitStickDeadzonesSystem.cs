using System.Diagnostics;
using Entitas;
using MercuryEngine.Input;
using MercuryEngine.Meta;

namespace MercuryEngine.Config {
  public sealed class EmitStickDeadzonesSystem : IInitializeSystem {
    private readonly IGroup<ConfigEntity> preferences;
    private readonly IGroup<InputEntity> sticks;

    public EmitStickDeadzonesSystem(Contexts contexts) {
      this.preferences = contexts.config.GetGroup(
        ConfigMatcher.AllOf(ConfigMatcher.DeviceIndex, ConfigMatcher.PlayerPreferences)
      );
      this.sticks = contexts.input.GetGroup(
        InputMatcher.AllOf(InputMatcher.DeviceIndex, InputMatcher.StickLabel)
      );
    }

    public void Initialize() {
      foreach (var prefsEntity in this.preferences) {
        var prefs = prefsEntity.playerPreferences.instance;
        foreach (var stickEntity in this.sticks) {
          if (stickEntity.deviceIndex.value != prefsEntity.deviceIndex.value) continue;

          void AddDeadzones(float inner, float outer) {
            stickEntity.AddStickDeadzones(inner, outer);
          }

          var label = stickEntity.stickLabel.value;
          switch (label) {
            case StickLabel.RightAnalog:
              AddDeadzones(prefs.rightStickInnerDeadzone, prefs.rightStickOuterDeadzone);
              break;
            case StickLabel.LeftAnalog:
              AddDeadzones(prefs.leftStickInnerDeadzone, prefs.leftStickOuterDeadzone);
              break;
            case StickLabel.DPad:
              AddDeadzones(0, 1);
              break;
            default:
              Debug.Fail(DiagnosticsHelper.UnknownEnumValueMessage(label));
              break;
          }
        }
      }
    }
  }
}