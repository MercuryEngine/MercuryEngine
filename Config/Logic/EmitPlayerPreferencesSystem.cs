using Entitas;

namespace MercuryEngine.Config {
  public sealed class EmitPlayerPreferencesSystem : IInitializeSystem {
    private readonly MetaContext meta;
    private readonly ConfigContext config;

    public EmitPlayerPreferencesSystem(Contexts contexts) {
      this.meta = contexts.meta;
      this.config = contexts.config;
    }

    public void Initialize() {
      var service = this.meta.configService.instance;
      var i = 0;
      foreach (var guid in service.GetPlayerGuids()) {
        var prefs = service.GetPreferences(guid);
        var entity = this.config.CreateEntity();
        entity.AddPlayerPreferences(prefs);
        entity.AddPlayerPreferencesIndex(i);
        i++;
      }
    }
  }
}