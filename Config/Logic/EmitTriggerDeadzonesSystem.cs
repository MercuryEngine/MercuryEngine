using System.Diagnostics;
using Entitas;
using MercuryEngine.Input;
using MercuryEngine.Meta;

namespace MercuryEngine.Config {
  public sealed class EmitTriggerDeadzonesSystem : IInitializeSystem {
    private readonly IGroup<ConfigEntity> preferences;
    private readonly IGroup<InputEntity> triggers;

    public EmitTriggerDeadzonesSystem(Contexts contexts) {
      this.preferences = contexts.config.GetGroup(
        ConfigMatcher.AllOf(ConfigMatcher.DeviceIndex, ConfigMatcher.PlayerPreferences)
      );
      this.triggers = contexts.input.GetGroup(
        InputMatcher.AllOf(InputMatcher.DeviceIndex, InputMatcher.TriggerLabel)
      );
    }

    public void Initialize() {
      foreach (var prefsEntity in this.preferences) {
        var prefs = prefsEntity.playerPreferences.instance;
        foreach (var triggerEntity in this.triggers) {
          if (triggerEntity.deviceIndex.value != prefsEntity.deviceIndex.value) continue;

          void AddDeadzones(float min, float max) {
            triggerEntity.AddTriggerDeadzones(min, max);
          }

          var label = triggerEntity.triggerLabel.value;
          switch (label) {
            case TriggerLabel.RightTrigger:
              AddDeadzones(prefs.rightTriggerMin, prefs.rightTriggerMax);
              break;
            case TriggerLabel.LeftTrigger:
              AddDeadzones(prefs.leftTriggerMin, prefs.leftTriggerMax);
              break;
            default:
              Debug.Fail(DiagnosticsHelper.UnknownEnumValueMessage(label));
              break;
          }
        }
      }
    }
  }
}