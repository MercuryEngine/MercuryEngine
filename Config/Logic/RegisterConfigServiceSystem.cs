using Entitas;

namespace MercuryEngine.Config {
  public sealed class RegisterConfigServiceSystem : IInitializeSystem {
    private readonly MetaContext context;
    private readonly IConfigService service;

    public RegisterConfigServiceSystem(Contexts contexts, IConfigService service) {
      this.context = contexts.meta;
      this.service = service;
    }

    public void Initialize() {
      this.context.SetConfigService(this.service);
    }
  }
}