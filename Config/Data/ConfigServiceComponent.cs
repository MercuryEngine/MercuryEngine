using Entitas.CodeGeneration.Attributes;

namespace MercuryEngine.Config {
  [Meta, Unique]
  public sealed class ConfigServiceComponent : Entitas.IComponent {
    public IConfigService instance;
  }
}