using Entitas.CodeGeneration.Attributes;

namespace MercuryEngine.Config {
  [Config, Input]
  public sealed class PlayerPreferencesIndexComponent : Entitas.IComponent {
    [PrimaryEntityIndex] public int value;
  }
}