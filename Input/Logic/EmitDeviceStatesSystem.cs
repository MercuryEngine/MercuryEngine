using Entitas;

namespace MercuryEngine.Input {
  public sealed class EmitDeviceStatesSystem : IExecuteSystem {
    private readonly InputContext input;
    private readonly MetaContext meta;
    private readonly IGroup<InputEntity> states;

    public EmitDeviceStatesSystem(Contexts contexts) {
      this.input = contexts.input;
      this.meta = contexts.meta;
      this.states = this.input.GetGroup(InputMatcher.DeviceState);
    }

    public void Execute() {
      var service = this.meta.inputService.instance;
      service.Poll();
      for (var i = 0; i < service.count; i++) {
        var state = service.GetState(i);
        var entity = this.input.CreateEntity();
        entity.AddDeviceIndex(i);
        entity.AddDeviceState(state);
        entity.shouldDestroy = true;
      }
    }
  }
}