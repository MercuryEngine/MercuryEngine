using System.Collections.Generic;
using Entitas;
using MercuryEngine.Utility;

namespace MercuryEngine.Input {
  public sealed class NormalizeSticksSystem : ReactiveSystem<InputEntity> {
    public NormalizeSticksSystem(Contexts contexts) : base(contexts.input) { }

    protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context) {
      return context.CreateCollector(InputMatcher.RawStickState);
    }

    protected override bool Filter(InputEntity entity) {
      return entity.hasRawStickState && entity.hasStickDeadzones;
    }

    protected override void Execute(List<InputEntity> entities) {
      foreach (var stick in entities) {
        var rawX = stick.rawStickState.x;
        var rawY = stick.rawStickState.y;

        var rawMagnitude = stick.rawStickState.magnitude;
        if (rawMagnitude <= stick.stickDeadzones.inner) {
          stick.ReplaceNormalizedStickState(0, 0, 0, 0);
        }
        else {
          var outerAdjustedMagnitude = rawMagnitude / stick.stickDeadzones.outer;
          var outerAdjustedInner = stick.stickDeadzones.inner / stick.stickDeadzones.outer;

          var magnitude = (outerAdjustedMagnitude - outerAdjustedInner) / (1 - outerAdjustedInner);
          magnitude = MathHelper.Clamp01(magnitude);
          var (unitX, unitY) = MathHelper.Normalize(rawX, rawY);
          var x = unitX * magnitude;
          var y = unitY * magnitude;
          var angle = MathHelper.Atan2(y, x);
          stick.ReplaceNormalizedStickState(angle, x, y, magnitude);
        }
      }
    }
  }
}