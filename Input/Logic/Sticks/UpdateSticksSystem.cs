using System.Diagnostics;
using Entitas;
using MercuryEngine.Meta;
using MercuryEngine.Utility;

namespace MercuryEngine.Input {
  public sealed class UpdateSticksSystem : IExecuteSystem {
    private readonly IGroup<InputEntity> sticks, states;

    public UpdateSticksSystem(Contexts contexts) {
      this.states = contexts.input.GetGroup(
        InputMatcher.AllOf(InputMatcher.DeviceIndex, InputMatcher.DeviceState)
      );
      this.sticks = contexts.input.GetGroup(
        InputMatcher.AllOf(InputMatcher.DeviceIndex, InputMatcher.StickLabel)
      );
    }

    public void Execute() {
      foreach (var stateEntity in this.states) {
        var state = stateEntity.deviceState.value;
        foreach (var stickEntity in this.sticks) {
          if (stickEntity.deviceIndex.value != stateEntity.deviceIndex.value) continue;

          void ReplaceRawStickState(float x, float y) {
            stickEntity.ReplaceRawStickState(x, y, MathHelper.Magnitude(x, y));
          }

          var label = stickEntity.stickLabel.value;
          switch (label) {
            case StickLabel.RightAnalog:
              ReplaceRawStickState(state.rightStickX, state.rightStickY);
              break;
            case StickLabel.LeftAnalog:
              ReplaceRawStickState(state.leftStickX, state.leftStickY);
              break;
            case StickLabel.DPad:

              float ButtonsAsStick(bool negative, bool positive) {
                return positive
                  ? negative
                    ? 0
                    : 1
                  : negative
                    ? -1
                    : 0;
              }

              var x = ButtonsAsStick(state.dPadLeft, state.dPadRight);
              var y = ButtonsAsStick(state.dPadDown, state.dPadUp);
              ReplaceRawStickState(x, y);
              break;
            default:
              Debug.Fail(DiagnosticsHelper.UnknownEnumValueMessage(label));
              break;
          }
        }
      }
    }
  }
}