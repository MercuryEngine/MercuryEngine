using Entitas;

namespace MercuryEngine.Input {
  public sealed class ClampStickDeadzonesSystem : ClampDeadzonesSystem<InputEntity> {
    public ClampStickDeadzonesSystem(Contexts contexts)
      : base(0, 1, contexts.input) { }

    protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context) {
      return context.CreateCollector(InputMatcher.StickDeadzones);
    }

    protected override bool Filter(InputEntity entity) => entity.hasStickDeadzones;

    protected override (float min, float max) GetDeadzones(InputEntity entity) {
      return (entity.stickDeadzones.inner, entity.stickDeadzones.outer);
    }

    protected override void ReplaceDeadzones(InputEntity entity, float min, float max) {
      entity.ReplaceStickDeadzones(min, max);
    }
  }
}