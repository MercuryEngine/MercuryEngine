using Entitas;

namespace MercuryEngine.Input {
  public sealed class RegisterInputServiceSystem : IInitializeSystem {
    private readonly MetaContext context;
    private readonly IInputService service;

    public RegisterInputServiceSystem(Contexts contexts, IInputService service) {
      this.context = contexts.meta;
      this.service = service;
    }

    public void Initialize() {
      this.context.SetInputService(this.service);
    }
  }
}