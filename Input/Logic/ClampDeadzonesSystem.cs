using System.Collections.Generic;
using Entitas;
using MercuryEngine.Utility;

namespace MercuryEngine.Input {
  public abstract class ClampDeadzonesSystem<TEntity> : ReactiveSystem<TEntity> where TEntity : class, IEntity {
    protected abstract (float min, float max) GetDeadzones(TEntity entity);
    protected abstract void ReplaceDeadzones(TEntity entity, float min, float max);

    private float floor;
    private float ceiling;

    private void SetDeadzones(float min, float max) {
      this.floor = min;
      this.ceiling = max;
    }

    protected ClampDeadzonesSystem(float floor, float ceiling,
                                   IContext<TEntity> context) : base(context) {
      SetDeadzones(floor, ceiling);
    }

    protected override void Execute(List<TEntity> entities) {
      foreach (var entity in entities) {
        var (min, max) = GetDeadzones(entity);
        (min, max) = MathHelper.ClampDeadzones(min, max, this.floor, this.ceiling);
        ReplaceDeadzones(entity, min, max);
      }
    }
  }
}