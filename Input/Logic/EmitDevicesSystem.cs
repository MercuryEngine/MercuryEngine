using Entitas;

namespace MercuryEngine.Input {
  public sealed class EmitDevicesSystem : IInitializeSystem {
    private readonly MetaContext meta;
    private readonly InputContext input;

    public EmitDevicesSystem(Contexts contexts) {
      this.meta = contexts.meta;
      this.input = contexts.input;
    }

    public void Initialize() {
      //TODO: Only supported buttons
      //TODO: Stick clicks?
      var count = this.meta.inputService.instance.count;
      for (var id = 0; id < count; id++) {
        void CreateStick(StickLabel label) => CreateInputDeviceEntity().AddStickLabel(label);
        void CreateTrigger(TriggerLabel label) => CreateInputDeviceEntity().AddTriggerLabel(label);
        void CreateButton(ButtonLabel label) => CreateInputDeviceEntity().AddButtonLabel(label);

        InputEntity CreateInputDeviceEntity() {
          var deviceEntity = this.input.CreateEntity();
          deviceEntity.AddDeviceIndex(id);
          return deviceEntity;
        }

        CreateStick(StickLabel.LeftAnalog);
        CreateStick(StickLabel.RightAnalog);
        CreateStick(StickLabel.DPad);

        CreateTrigger(TriggerLabel.LeftTrigger);
        CreateTrigger(TriggerLabel.RightTrigger);

        CreateButton(ButtonLabel.A);
        CreateButton(ButtonLabel.B);
        CreateButton(ButtonLabel.X);
        CreateButton(ButtonLabel.Y);
        CreateButton(ButtonLabel.Z);

        CreateButton(ButtonLabel.LeftShoulder);
        CreateButton(ButtonLabel.RightShoulder);

        CreateButton(ButtonLabel.DPadLeft);
        CreateButton(ButtonLabel.DPadRight);
        CreateButton(ButtonLabel.DPadDown);
        CreateButton(ButtonLabel.DPadUp);

        CreateButton(ButtonLabel.Start);
        CreateButton(ButtonLabel.Select);
        CreateButton(ButtonLabel.Home);
      }
    }
  }
}