using System.Collections.Generic;
using Entitas;
using MercuryEngine.Utility;

namespace MercuryEngine.Input {
  public sealed class NormalizeTriggersSystem : ReactiveSystem<InputEntity> {
    public NormalizeTriggersSystem(Contexts contexts) : base(contexts.input) { }

    protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context) {
      return context.CreateCollector(InputMatcher.RawTriggerState);
    }

    protected override bool Filter(InputEntity entity) {
      return entity.hasRawTriggerState && entity.hasTriggerDeadzones;
    }

    protected override void Execute(List<InputEntity> entities) {
      foreach (var trigger in entities) {
        var min = trigger.triggerDeadzones.min;
        var max = trigger.triggerDeadzones.max;
        var value = trigger.rawTriggerState.value;

        value = value < min
          ? 0
          : MathHelper.Clamp01((value - min) / (max - min));

        trigger.ReplaceNormalizedTriggerState(value);
      }
    }
  }
}