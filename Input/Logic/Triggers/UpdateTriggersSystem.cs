using System.Diagnostics;
using Entitas;
using MercuryEngine.Meta;

namespace MercuryEngine.Input {
  public sealed class UpdateTriggersSystem : IExecuteSystem {
    private readonly IGroup<InputEntity> triggers, states;

    public UpdateTriggersSystem(Contexts contexts) {
      this.states = contexts.input.GetGroup(
        InputMatcher.AllOf(InputMatcher.DeviceIndex, InputMatcher.DeviceState)
      );
      this.triggers = contexts.input.GetGroup(
        InputMatcher.AllOf(InputMatcher.DeviceIndex, InputMatcher.TriggerLabel)
      );
    }

    public void Execute() {
      foreach (var stateEntity in this.states) {
        var state = stateEntity.deviceState.value;
        foreach (var triggerEntity in this.triggers) {
          if (triggerEntity.deviceIndex.value != stateEntity.deviceIndex.value) continue;

          void ReplaceRawTriggerState(float value) {
            triggerEntity.ReplaceRawTriggerState(value);
          }

          var label = triggerEntity.triggerLabel.value;
          switch (label) {
            case TriggerLabel.RightTrigger:
              ReplaceRawTriggerState(state.rightTrigger);
              break;
            case TriggerLabel.LeftTrigger:
              ReplaceRawTriggerState(state.leftTrigger);
              break;
            default:
              Debug.Fail(DiagnosticsHelper.UnknownEnumValueMessage(label));
              break;
          }
        }
      }
    }
  }
}