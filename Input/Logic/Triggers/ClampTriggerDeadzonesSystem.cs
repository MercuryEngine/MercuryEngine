using Entitas;

namespace MercuryEngine.Input {
  public sealed class ClampTriggerDeadzonesSystem : ClampDeadzonesSystem<InputEntity> {
    public ClampTriggerDeadzonesSystem(Contexts contexts)
      : base(-1, 1, contexts.input) { }

    protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context) {
      return context.CreateCollector(InputMatcher.TriggerDeadzones);
    }

    protected override bool Filter(InputEntity entity) {
      return entity.hasTriggerDeadzones;
    }

    protected override (float min, float max) GetDeadzones(InputEntity entity) {
      return (entity.triggerDeadzones.min, entity.triggerDeadzones.max);
    }

    protected override void ReplaceDeadzones(InputEntity entity, float min, float max) {
      entity.ReplaceTriggerDeadzones(min, max);
    }
  }
}