public partial class InputEntity : MercuryEngine.IDestroyableEntity { }

namespace MercuryEngine.Input {
  public sealed class InputDestroySystem : DestroySystem<InputEntity> {
    public InputDestroySystem(Contexts contexts) : base(contexts.input, InputMatcher.Destroy) { }
  }
}