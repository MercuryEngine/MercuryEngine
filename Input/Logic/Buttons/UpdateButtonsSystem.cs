using System.Diagnostics;
using Entitas;
using MercuryEngine.Meta;

namespace MercuryEngine.Input {
  public sealed class UpdateButtonsSystem : IExecuteSystem {
    private readonly IGroup<InputEntity> buttons, states;

    public UpdateButtonsSystem(Contexts contexts) {
      this.states = contexts.input.GetGroup(
        InputMatcher.AllOf(InputMatcher.DeviceIndex, InputMatcher.DeviceState)
      );
      this.buttons = contexts.input.GetGroup(
        InputMatcher.AllOf(InputMatcher.DeviceIndex, InputMatcher.ButtonLabel)
      );
    }

    public void Execute() {
      foreach (var stateEntity in this.states) {
        var state = stateEntity.deviceState.value;
        foreach (var buttonEntity in this.buttons) {
          if (buttonEntity.deviceIndex.value != stateEntity.deviceIndex.value) continue;

          void SetButton(bool isDown) {
            buttonEntity.isButtonDown = isDown;
          }

          var label = buttonEntity.buttonLabel.value;
          switch (label) {
            case ButtonLabel.A:
              SetButton(state.a);
              break;
            case ButtonLabel.B:
              SetButton(state.b);
              break;
            case ButtonLabel.X:
              SetButton(state.x);
              break;
            case ButtonLabel.Y:
              SetButton(state.y);
              break;
            case ButtonLabel.Z:
              SetButton(state.z);
              break;
            case ButtonLabel.LeftShoulder:
              SetButton(state.leftShoulder);
              break;
            case ButtonLabel.RightShoulder:
              SetButton(state.rightShoulder);
              break;
            case ButtonLabel.DPadLeft:
              SetButton(state.dPadLeft);
              break;
            case ButtonLabel.DPadRight:
              SetButton(state.dPadRight);
              break;
            case ButtonLabel.DPadDown:
              SetButton(state.dPadDown);
              break;
            case ButtonLabel.DPadUp:
              SetButton(state.dPadUp);
              break;
            case ButtonLabel.Start:
              SetButton(state.start);
              break;
            case ButtonLabel.Select:
              SetButton(state.@select);
              break;
            case ButtonLabel.Home:
              SetButton(state.home);
              break;
            default:
              Debug.Fail(DiagnosticsHelper.UnknownEnumValueMessage(label));
              break;
          }
        }
      }
    }
  }
}