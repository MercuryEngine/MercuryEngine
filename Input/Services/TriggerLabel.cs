namespace MercuryEngine.Input {
  public enum TriggerLabel {
    LeftTrigger,
    RightTrigger
  }
}