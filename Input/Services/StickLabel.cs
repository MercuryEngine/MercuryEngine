namespace MercuryEngine.Input {
  public enum StickLabel {
    RightAnalog = 1,
    LeftAnalog,
    DPad,
  }
}