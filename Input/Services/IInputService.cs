namespace MercuryEngine.Input {
  public interface IInputService {
    int count { get; }
    void Poll();
    DeviceState GetState(int index);
  }
}