namespace MercuryEngine.Input {
  public enum ButtonLabel {
    A = 1,
    B,
    X,
    Y,
    Z,
    LeftShoulder,
    RightShoulder,
    DPadLeft,
    DPadRight,
    DPadUp,
    DPadDown,
    Start,
    Select,
    Home
  }
}