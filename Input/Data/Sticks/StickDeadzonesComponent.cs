using Entitas;

namespace MercuryEngine.Input {
  [Input]
  public sealed class StickDeadzonesComponent : IComponent {
    public float inner, outer;
  }
}