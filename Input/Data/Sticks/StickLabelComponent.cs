using Entitas;
using Entitas.CodeGeneration.Attributes;

namespace MercuryEngine.Input {
  [Input]
  public sealed class StickLabelComponent : IComponent {
    [EntityIndex] public StickLabel value;
  }
}