namespace MercuryEngine.Input {
  [Input]
  public sealed class NormalizedStickStateComponent : StickStateComponent {
    public float angleRadians;
  }
}