using Entitas;

namespace MercuryEngine.Input {
  public abstract class StickStateComponent : IComponent {
    public float x { get; set; }
    public float y { get; set; }
    public float magnitude { get; set; }
  }
}