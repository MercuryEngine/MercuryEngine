using Entitas.CodeGeneration.Attributes;

namespace MercuryEngine.Input {
  [Input]
  public sealed class ButtonLabelComponent : Entitas.IComponent {
    [EntityIndex] public ButtonLabel value;
  }
}