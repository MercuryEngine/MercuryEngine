namespace MercuryEngine.Input {
  public readonly struct DeviceState {
    public readonly float leftStickX;
    public readonly float leftStickY;
    public readonly float rightStickX;
    public readonly float rightStickY;

    public readonly float leftTrigger;
    public readonly float rightTrigger;

    public readonly bool leftShoulder;
    public readonly bool rightShoulder;

    public readonly bool a;
    public readonly bool b;
    public readonly bool x;
    public readonly bool y;
    public readonly bool z;

    public readonly bool dPadLeft;
    public readonly bool dPadRight;
    public readonly bool dPadDown;
    public readonly bool dPadUp;

    public readonly bool start;
    public readonly bool @select;
    public readonly bool home;

    public DeviceState(float leftStickX, float leftStickY, float rightStickX, float rightStickY,
                       float leftTrigger, float rightTrigger, bool leftShoulder, bool rightShoulder,
                       bool a, bool b, bool x, bool y, bool z,
                       bool dPadLeft, bool dPadRight, bool dPadDown, bool dPadUp,
                       bool start, bool @select, bool home) {
      this.leftStickX = leftStickX;
      this.leftStickY = leftStickY;
      this.rightStickX = rightStickX;
      this.rightStickY = rightStickY;
      this.leftTrigger = leftTrigger;
      this.rightTrigger = rightTrigger;
      this.leftShoulder = leftShoulder;
      this.rightShoulder = rightShoulder;
      this.a = a;
      this.b = b;
      this.x = x;
      this.y = y;
      this.z = z;
      this.dPadLeft = dPadLeft;
      this.dPadRight = dPadRight;
      this.dPadDown = dPadDown;
      this.dPadUp = dPadUp;
      this.start = start;
      this.@select = @select;
      this.home = home;
    }
  }
}