namespace MercuryEngine.Input {
  [Input]
  public sealed class DeviceStateComponent : Entitas.IComponent {
    public DeviceState value;
  }
}