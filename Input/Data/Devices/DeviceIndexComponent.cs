using Entitas.CodeGeneration.Attributes;

namespace MercuryEngine.Input {
  [Config, Input, Physics]
  public sealed class DeviceIndexComponent : Entitas.IComponent {
    [EntityIndex] public int value;
  }
}