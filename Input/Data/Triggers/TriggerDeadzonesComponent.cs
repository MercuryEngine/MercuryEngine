using Entitas;

namespace MercuryEngine.Input {
  [Input]
  public sealed class TriggerDeadzonesComponent : IComponent {
    public float min, max;
  }
}