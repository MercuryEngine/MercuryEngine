using Entitas;

namespace MercuryEngine.Input {
  [Input]
  public sealed class TriggerLabelComponent : IComponent {
    public TriggerLabel value;
  }
}