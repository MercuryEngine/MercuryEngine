using Entitas;

namespace MercuryEngine.Input {
  [Input]
  public abstract class TriggerStateComponent : IComponent {
    public float value { get; set; }
  }
}