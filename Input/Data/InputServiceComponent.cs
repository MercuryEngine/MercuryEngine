using Entitas.CodeGeneration.Attributes;

namespace MercuryEngine.Input {
  [Meta, Unique]
  public sealed class InputServiceComponent : Entitas.IComponent {
    public IInputService instance;
  }
}